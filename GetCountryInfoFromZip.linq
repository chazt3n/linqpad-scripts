<Query Kind="Program">
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>EntityFramework</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>RestSharp</Namespace>
</Query>

void Main()
{
	//Define a zip code variable (You can get this from the request)
	int zipCode = 37211;
	
	//Create a new REST Client
	string baseUrl = "http://zip.getziptastic.com/v2/US";
	RestClient client = new RestClient(baseUrl);
	
	//Add your zipcode, along with a leading forward slash, as the request's resource. This will be appended to the client's base url. (eg: http://zip.getziptastic.com/v2/US/37211)
	IRestRequest request = new RestRequest(String.Format("/{0}", zipCode));
	
	IRestResponse response = client.Execute(request);
	
	response.Content.Dump("Response Content");
}

// Define other methods and classes here
