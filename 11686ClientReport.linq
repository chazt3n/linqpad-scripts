<Query Kind="Statements">
  <Connection>
    <ID>edea8933-16a2-46f0-a2c1-72cb6b959667</ID>
    <Persist>true</Persist>
    <Server>cpsqldbkp01vn02</Server>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <Reference>&lt;RuntimeDirectory&gt;\System.Linq.dll</Reference>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>EntityFramework</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>System.Data.Entity</Namespace>
  <Namespace>System.Linq</Namespace>
</Query>

var today = DateTime.Now.Date;
var job = Jobs.First(j => j.JobSourceContactId != null);//.Dump("Job");
var contactId = job.JobSourceContactId;


var contact = Contacts.First(c => c.ContactId == contactId);//.Dump("Contact");

//For each contact
//'Referrals in the last 12 months' Column
var jobsFromClient = contact.JobSourceJobs.Where(j => j.FNOLReceivedOn < today && j.FNOLReceivedOn > today.AddDays(- 365)).Dump("Jobs that the Contact referred this year");
var numberOfReferralsInThePastYear = jobsFromClient.Count().Dump("Number of referrals from the past year");

//'Revenue billed in the last 12 months' Column
//var billedJobsFromClient
var unpaidJobsFromClient = jobsFromClient.Where(jfc => jfc.JobTaskProgresses.All(jtp => !jtp.JobTaskTemplate.JobTaskTemplateDescription.Contains("Paid"))).Dump("Unpaid jobs");

//'Revenue collected in the last 12 months' Column
var paidJobsFromClient = jobsFromClient.Where(jfc => jfc.JobTaskProgresses.FirstOrDefault(jtp => jtp.JobTaskTemplate.JobTaskTemplateDescription.Contains("Paid")) != null).Dump("Paid jobs");
var collectedRevenues = paidJobsFromClient.SelectMany(pj => pj.JobSummaries.Select(j => j.JobRevenue)).Dump("Collected Revenue");
decimal totalCollectedRevenue = 0;
collectedRevenues.Each(x => totalCollectedRevenue += x.HasValue == true ? x.Value : (decimal) 0);
totalCollectedRevenue.Dump("totalRevenue");

//'Date of last referral' Column
var dateOfLastReferral = jobsFromClient.OrderByDescending(j => j.FNOLReceivedOn).Select(x => x.FNOLReceivedOn).FirstOrDefault().Dump("Date of Last Referral");

//'Customer name of last referral' Column
var lastReferralCustomer = jobsFromClient.OrderByDescending(j => j.FNOLReceivedOn).FirstOrDefault().Customer;
var lastReferralCustomerName = lastReferralCustomer.FirstName +" "+ lastReferralCustomer.LastName;
lastReferralCustomerName.Dump("Last Referral Customer");