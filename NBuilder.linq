<Query Kind="Program">
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>FizzWare.NBuilder</Namespace>
</Query>

void Main()
{
	IEnumerable<Contact> contacts = Builder<Contact>.CreateListOfSize(10).WhereRandom(3).Have(x => x.kids = new List<string>(){"test"}).Build();
	
	CenterOfInfluence coi = new CenterOfInfluence();
	
	coi.Contacts = contacts;
	coi.Contacts.Dump("contacts");
	
	
	
}

// Define other methods and classes here
public class Contact
{
	public string FName { get; set; }
	public string LName { get; set; }
	public IEnumerable<string> kids {get;set;}
}

public class CenterOfInfluence
{
	public CenterOfInfluence()
	{
		Contacts = new List<Contact>();
	}
	public IEnumerable<Contact> Contacts {get;set;}
}

public class Activity
{
	public string Foo { get; set; }
}