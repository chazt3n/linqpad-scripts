<Query Kind="Program">
  <Connection>
    <ID>a79d2235-06fb-49d3-a562-0955b48c46c2</ID>
    <Persist>true</Persist>
    <Server>cpsqlxiit01vn01\wcom</Server>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <Reference>C:\Projects\WorkCenters\WorkCenter\Main\Servpro.WorkCenters.Data\bin\Debug\Servpro.Common.Contracts.dll</Reference>
  <Reference>C:\Projects\WorkCenters\WorkCenter\Main\Servpro.WorkCenters.Data\bin\Debug\Servpro.Common.dll</Reference>
  <Reference>C:\Projects\WorkCenters\WorkCenter\Main\Servpro.WorkCenters.Business.Entities\bin\Debug\Servpro.WorkCenters.Business.Entities.dll</Reference>
  <Reference>C:\Projects\WorkCenters\WorkCenter\Main\Servpro.WorkCenters.Data\bin\Debug\Servpro.WorkCenters.Data.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.dll</Reference>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>EntityFramework</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>Servpro.WorkCenters.Business.Entities</Namespace>
  <Namespace>Servpro.WorkCenters.Data</Namespace>
  <Namespace>System.Data.Entity</Namespace>
</Query>

void Main()
{
	           using (var entityContext = new WorkCentersContext())
	            {
	                Servpro.WorkCenters.Business.Entities.PurchaseOrder purchaseOrder = entityContext.PurchaseOrderSet
	                    .Include(x => x.FranchiseOperation)
	                    .Include(x => x.CenterOfInfluence.CenterOfInfluenceContacts.Select(c => c.Contact.Connections))
	                    .Include(x => x.CenterOfInfluence.CenterOfInfluenceContacts.Select(c => c.Contact.PhoneNumbers))
	                    .Include(x => x.CenterOfInfluence.Organization.Addresses)
	                    .Include(x => x.CenterOfInfluence.Organization.Connections)
	                    .Include(x => x.CenterOfInfluence.Organization.PhoneNumbers)
	                    .Include(x => x.PurchaseOrderItems)
	                    .Where(x => x.Id == 1);
	
	                purchaseOrder.Dump("purchaseOrder");
	            }
				
				using (var entityContext = new WorkCentersContext())
	            {
	                Servpro.WorkCenters.Business.Entities.PurchaseOrder purchaseOrder = entityContext.PurchaseOrderSet
	                    .Where(x => x.Id == 1)
						.Include(x => x.FranchiseOperation)
	                    .Include(x => x.CenterOfInfluence.CenterOfInfluenceContacts.Select(c => c.Contact.Connections))
	                    .Include(x => x.CenterOfInfluence.CenterOfInfluenceContacts.Select(c => c.Contact.PhoneNumbers))
	                    .Include(x => x.CenterOfInfluence.Organization.Addresses)
	                    .Include(x => x.CenterOfInfluence.Organization.Connections)
	                    .Include(x => x.CenterOfInfluence.Organization.PhoneNumbers)
	                    .Include(x => x.PurchaseOrderItems).First();
	                purchaseOrder.Id.Dump("purchaseOrder");
	            }
}

// Define other methods and classes here