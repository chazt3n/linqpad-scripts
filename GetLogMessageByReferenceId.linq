<Query Kind="Statements">
  <Connection>
    <ID>73eb6351-34c2-4579-80dc-57b7a9ddfcf8</ID>
    <Persist>true</Persist>
    <Server>CPSQLDBKP01VN01</Server>
    <Database>WorkCenterLog</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>EntityFramework</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
</Query>

var referenceId = "773d83f9-6672-40e4-b4a2-fbffaa15dbf5";
ApplicationLogs.Single(log => log.Message.Contains(referenceId)).Dump();