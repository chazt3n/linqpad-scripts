<Query Kind="Expression">
  <Connection>
    <ID>a79d2235-06fb-49d3-a562-0955b48c46c2</ID>
    <Persist>true</Persist>
    <Server>cpsqlxiit01vn01\wcom</Server>
    <IsProduction>true</IsProduction>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
</Query>

CentersOfInfluenceContact.OrderByDescending(c => c.Contact.ContactSpecialOccasions.FirstOrDefault().SpecialOccasionDate)