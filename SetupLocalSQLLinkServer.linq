<Query Kind="SQL">
  <Connection>
    <ID>a71a05fb-05f1-4103-8507-d7e523ffc6b3</ID>
    <Persist>true</Persist>
    <Server>.</Server>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
</Query>

USE [master]
GO
IF EXISTS (SELECT name FROM sys.servers WHERE (server_id != 0) AND [name] = N'lsEvergreenData')
BEGIN
	EXEC sp_dropserver @server='lsEvergreenData'
END
ELSE
BEGIN
	PRINT 'DO SOMETHING ELSE'
END


/****** Object:  LinkedServer [lsEvergreenData]    Script Date: 10/17/2014 2:36:53 PM ******/
EXEC master.dbo.sp_addlinkedserver @server = N'lsEvergreenData', @srvproduct=N'sql_server', @provider=N'SQLNCLI11', @datasrc=N'.'
/* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'lsEvergreenData',@useself=N'True',@locallogin=NULL,@rmtuser=NULL,@rmtpassword=NULL

GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'lsEvergreenData', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO

USE [EvergreenData]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'FranchiseCopyContentMap') AND type in (N'U'))
BEGIN
       DROP TABLE [dbo].[FranchiseCopyContentMap]
END

GO

CREATE TABLE [dbo].[FranchiseCopyContentMap](
       [FranchiseCopyContentMapId] [uniqueidentifier] NOT NULL,
       [TableName] [varchar](255) NULL,
       [FieldName] [varchar](255) NULL,
       [SourceId] [uniqueidentifier] NULL,
       [SourceFranchiseId] [uniqueidentifier] NULL,
       [SourceFranchiseSetId] [uniqueidentifier] NULL,
       [TargetId] [uniqueidentifier] NULL,
       [TargetFranchiseId] [uniqueidentifier] NULL,
       [TargetFranchiseSetId] [uniqueidentifier] NULL,
       [FranchiseCopyContentId] [uniqueidentifier] NULL,
       [ExecutionDate] [datetime] NULL,
       [TargetRowId] [int] NULL,
       [SourceRowId] [int] NULL,
CONSTRAINT [PK_FranchiseCopyContentMap] PRIMARY KEY CLUSTERED 
(
       [FranchiseCopyContentMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'FranchiseCopyContentMapTable') AND type in (N'U'))
BEGIN
       DROP TABLE [dbo].[FranchiseCopyContentMapTable]
END

GO

CREATE TABLE [dbo].[FranchiseCopyContentMapTable](
       [TableName] [varchar](255) NOT NULL,
       [ProcessOrderIndex] [int] NULL,
CONSTRAINT [PK_FranchiseCopyContentMapTables] PRIMARY KEY CLUSTERED 
(
       [TableName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

