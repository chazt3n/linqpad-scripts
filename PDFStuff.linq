<Query Kind="Program">
  <Connection>
    <ID>a79d2235-06fb-49d3-a562-0955b48c46c2</ID>
    <Persist>true</Persist>
    <Server>cpsqlxiit01vn01\wcom</Server>
    <Database>WCOptimizations</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <NuGetReference>TuesPechkin</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>TuesPechkin</Namespace>
  <Namespace>System.Drawing</Namespace>
</Query>

void Main()
{
	Printer.GeneratePdf(@"<html><h1><div class=""Table""><div class=""Title""><p>This is a Table</p></div><div class=""Heading"">        <div class=""Cell"">         <p>Heading 1</p>       </div>       <div class=""Cell"">       <p>Heading 2</p>      </div>       <div class=""Cell"">          <p>Heading 3</p>        </div>    </div>    <div class=""Row"">        <div class=""Cell"">         <p>Row 1 Column 1</p>        </div>        <div class=""Cell"">         <p>Row 1 Column 2</p>        </div>        <div class=""Cell"">            <p>Row 1 Column 3</p>       </div>    </div>    <div class=""Row"">        <div class=""Cell"">            <p>Row 2 Column 1</p>        </div>        <div class=""Cell"">            <p>Row 2 Column 2</p>        </div>        <div class=""Cell"">            <p>Row 2 Column 3</p>        </div>    </div></div></h1><style type=""text/css"">    .Table    {        display: table;    }    .Title    {        display: table-caption;        text-align: center;        font-weight: bold;        font-size: larger;    }    .Heading    {        display: table-row;        font-weight: bold;        text-align: center;    }    .Row    {        display: table-row;    }    .Cell    {        display: table-cell;        border: solid;        border-width: thin;        padding-left: 5px;        padding-right: 5px;    }</style>  </html>");
//	Printer.GeneratePdf(@"<html><h1>Hello World</h1></html>");
}

// Define other methods and classes here
public static class Printer
{
    public const string HtmlToPdfExePath = @"C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe";

    public static bool GeneratePdf(string html)
    {
	    MemoryStream pdf = new MemoryStream();
	   	Size pageSize = new Size(800,480);
        Process p;
        StreamWriter stdin;
        ProcessStartInfo psi = new ProcessStartInfo();

        psi.FileName = HtmlToPdfExePath;
        psi.WorkingDirectory = Path.GetDirectoryName(psi.FileName);

        // run the conversion utility
        psi.UseShellExecute = false;
        psi.CreateNoWindow = true;
        psi.RedirectStandardInput = true;
        psi.RedirectStandardOutput = true;
        psi.RedirectStandardError = true;

        // note: that we tell wkhtmltopdf to be quiet and not run scripts
        psi.Arguments = "-q -n --disable-smart-shrinking " + (pageSize.IsEmpty? "" : "--page-width " + pageSize.Width +  "mm --page-height " + pageSize.Height + "mm") + " - -";

        p = Process.Start(psi);

        try {
            stdin = p.StandardInput;
            stdin.AutoFlush = true;
            stdin.Write(html);
            stdin.Dispose();

            CopyStream(p.StandardOutput.BaseStream, pdf);
            p.StandardOutput.Close();
            pdf.Position = 0;
			pdf.WriteTo(new FileStream(@"C:\Test.pdf",FileMode.OpenOrCreate));
            p.WaitForExit(10000);

            return true;
        } catch {
            return false;

        } finally {
            p.Dispose();
        }
    }

    public static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[32768];
        int read;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0) {
            output.Write(buffer, 0, read);
        }
    }
}