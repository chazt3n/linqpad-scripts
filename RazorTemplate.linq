<Query Kind="Program">
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference Version="2.1">RazorEngine</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>RazorEngine</Namespace>
</Query>

void Main()
{

	PurchaseOrder purchaseOrder = new PurchaseOrder();
	purchaseOrder.Business = new Business();
	purchaseOrder.Business.Name = "Best Buy";
	
	string fileTemplate = File.ReadAllText("Files\\pdfTemplate.html");
	string result = Razor.Parse(fileTemplate, new { FranchiseName = "North Irving", ReportType="Purchase Order", PurchaseOrderNumber = "PO1", CrewChief="John Smith", BusinessName="Best Buy", ContactName = "Chasten W", Address = "123 Test Drive", Phone = "123 123 1234", Amount = "5,250", Item = "This will be free form text, the user can type up to 250 characters.", Quantity="150", UnitPrice="35.00", Notes="Here are some notes about the purchase order, this text/ paragraph should allow 8,000 character maximum and implement a vertical scroll when needed..." });
	result.Dump();
}

// Define other methods and classes here
 public class PurchaseOrder
    {
        public int Id { get; set; }

        public virtual int FranchiseOperationId { get; set; }

        public virtual int JobId { get; set; }

        public virtual int BusinessId { get; set; }
		
		public virtual Business Business {get;set;}

        public virtual string PurchaseOrderNumber { get; set; }
        public virtual DateTime OrderDate { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string Notes { get; set; }

        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems { get; set; }
    }
	
	public class Business 
	{
		public string Name { get; set; }
	}
	
public class PurchaseOrderItem
    {
        public int Id { get; set; }

        public virtual int PurchaseOrderId { get; set; }
        public PurchaseOrder PurchaseOrder { get; set; }

        public virtual string Description { get; set; }
        public virtual decimal Quantity { get; set; }
        public virtual decimal Amount { get; set; }
    }