<Query Kind="Statements">
  <Connection>
    <ID>d662038a-e28e-4e67-a4cc-7a7775b69ae5</ID>
    <Persist>true</Persist>
    <Server>CPSQLDBKP01VN02</Server>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <Namespace>Autofac</Namespace>
</Query>

var visits = MarketingVisits.Where(x => x.MarketingRepId == 225 && x.FranchiseOperationId == 144).ToList();
visits.Dump("Marketing Visits to be updated");


var territory = MarketingTerritories.Single(m => m.MarketingTerritoryId == 46 && m.FranchiseOperationId == 144).Dump("Marketing Territory to be deleted");




var cois = CentersOfInfluence.Where(coi => coi.MarketingTerritoryId == 46 && coi.FranchiseOperationId == 144);
cois.Dump("Centers Of Influence to be updated");

MarketingRoutes.Where(x => x.MarketingTerritoryId == 46).Dump();

