<Query Kind="Program">
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <Namespace>Autofac</Namespace>
  <Namespace>RestSharp</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	RestClient client = new RestClient("https://workcenterqa.ssostaging.servpronet.com");
	
#region View Single Contact
	var viewSingleContactApiCalls = new List<RestRequest>{
		(new RestRequest("/Marketing/api/contact/business?id=2224",Method.GET)),
		(new RestRequest("/Marketing/api/business/isuser/smm",Method.GET)),
		(new RestRequest("/Marketing/api/business/types",Method.GET)),
		(new RestRequest("/Marketing/api/territory/territories",Method.GET)),
		(new RestRequest("/Marketing/api/business/insurancecos",Method.GET)),
		(new RestRequest("/Marketing/api/contact/getcountries",Method.GET)),
		(new RestRequest("/Marketing/api/business/insurancecos",Method.GET)),
		(new RestRequest("/Marketing/api/business/manage/2224",Method.GET)),
		(new RestRequest("/Marketing/api/business/routes?id=11",Method.GET)),
		(new RestRequest("/Marketing/api/common/states/82",Method.GET))
	};
#endregion

#region Marketing Home Page
	var marketingHomePageApiCalls = new List<RestRequest>{
		new RestRequest("/Marketing/api/common/filters/dates",Method.GET),
		new RestRequest("/Marketing/api/widgets/filters/reps",Method.GET),
		new RestRequest("/Marketing/api/widgets/weekly/top-prospects/0",Method.GET),
		new RestRequest("/Marketing/api/widgets/contact-summary/0",Method.GET),
		new RestRequest("/Marketing/api/widgets/prod-jobs/01-20-2015/0",Method.GET),
		new RestRequest("/Marketing/api/widgets/account-summary/01-01-2015/01-20-2015",Method.GET),
		new RestRequest("/Marketing/api/widgets/erp-analysis/01-01-2015/01-20-2015/0",Method.GET),
	};
#endregion	
	
#region Scheduler
	var schedulerApiCalls = new List<RestRequest>{
		new RestRequest("/Office/api/Jobs/Search/Status",Method.GET),
		new RestRequest("/Office/api/AlertsAndTasks/today/",Method.GET),
		new RestRequest("/Office/api/scheduler/employees/0",Method.GET),
		new RestRequest("/Office/api/scheduler/unscheduled-list",Method.GET),
		new RestRequest("/Office/api/AlertsAndTasks/Summary/TaskAlertCount/",Method.GET),
		new RestRequest("/Office/api/EquipmentAPI/EquipmentAlertCount/",Method.GET),
		new RestRequest("/Office/api/Footer/VersionInfo/",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=day&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-list?Range=allday&StartDate=2015-01-20T07%3A00%3A00-06%3A00&DepartmentId=0&RoleGroupId=0&AppointmentTypeId=0&EmployeeId=0",Method.GET),
		new RestRequest("/Office/api/scheduler/department-list",Method.GET),
		new RestRequest("/Office/api/scheduler/taskroute-list",Method.GET),
		new RestRequest("/Office/api/scheduler/taskroute-list",Method.GET),
		new RestRequest("/Office/api/ContactManagement/GetStates/82",Method.GET),
		new RestRequest("/Office/api/scheduler/subdepartment-list/4",Method.GET),
		new RestRequest("/Office/api/scheduler/JobReference",Method.GET),
		new RestRequest("/Office/api/scheduler/GetLossAddress/592269",Method.GET),
		new RestRequest("/Office/api/scheduler/detail",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-list?Range=allday&StartDate=2015-01-20T07%3A00%3A00-06%3A00&DepartmentId=0&RoleGroupId=0&AppointmentTypeId=0&EmployeeId=0",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=day&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/Jobs/WorkOrderType",Method.GET),
		new RestRequest("/Office/api/Jobs/WorkOrder/GetMaxWorkOrderId/592269",Method.GET),
		new RestRequest("/Office/api/Jobs/AddWorkOrder",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-list?Range=allday&StartDate=2015-01-20T07%3A00%3A00-06%3A00&DepartmentId=0&RoleGroupId=0&AppointmentTypeId=0&EmployeeId=0",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=day&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/scheduler/unscheduled-list",Method.GET),
		new RestRequest("/Office/api/scheduler/subdepartment-list/1",Method.GET),
		new RestRequest("/Office/api/scheduler/subdepartment-list/1",Method.GET),
		new RestRequest("/Office/api/scheduler/subdepartment-list/1",Method.GET),
		new RestRequest("/Office/api/AlertsAndTasks/Summary/TaskAlertCount/",Method.GET),
		new RestRequest("/Office/api/EquipmentAPI/EquipmentAlertCount/",Method.GET),
		new RestRequest("/Office/api/scheduler/subdepartment-list/2",Method.GET),
		new RestRequest("/Office/api/scheduler/detail/edit",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-list?Range=allday&StartDate=2015-01-20T07%3A00%3A00-06%3A00&DepartmentId=0&RoleGroupId=0&AppointmentTypeId=0&EmployeeId=0",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=day&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=night&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/scheduler/scheduled-json?StartDate=2015-01-20T00%3A00%3A00&Range=day&DepartmentId=&EmployeeId=&RoleGroupId=&AppointmentTypeId=&Departments=&CalendarScroll=undefined",Method.GET),
		new RestRequest("/Office/api/scheduler/JobReference/592243",Method.GET),
		new RestRequest("/Office/api/scheduler/GetLossAddress/592243",Method.GET),
	};	
#endregion

#region Wip Board
	var wipApiCalls = new List<RestRequest>{
		new RestRequest("/Office/api/Jobs/Search/Status",Method.GET),
		new RestRequest("/Office/api/wip-board/GetFranchise/",Method.GET),
		new RestRequest("/Office/api/wip-board/GetWipPhase/0/0/?skip=0&take=10&pageSize=10&page=1&sort%5B0%5D%5Bfield%5D=DaysOpen&sort%5B0%5D%5Bdir%5D=asc",Method.GET),
		new RestRequest("/Office/api/AlertsAndTasks/Summary/TaskAlertCount/",Method.GET),
		new RestRequest("/Office/api/EquipmentAPI/EquipmentAlertCount/",Method.GET),
		new RestRequest("/Office/api/Footer/VersionInfo/",Method.GET)
		};
#endregion	
	
	for (int i = 0; i < 100; i++)
	{
		var stopWatch = Stopwatch.StartNew();
		var iterator = i.ToString();
		
		Task.Factory.StartNew(x => {
		
			ExecuteRequests(client, viewSingleContactApiCalls);
//			ExecuteRequests(client, marketingHomePageApiCalls);
//			ExecuteRequests(client, schedulerApiCalls);
//			ExecuteRequests(client, wipApiCalls);
			
		},TaskContinuationOptions.LongRunning).ContinueWith(a => {
			stopWatch.Stop();	
			var elapsedSeconds = stopWatch.Elapsed.Seconds;
			Console.WriteLine("{0},{1}", iterator, elapsedSeconds);
		});
		
	}
	
	
}

public void ExecuteRequests(RestClient client, List<RestRequest> requests)
{
	Parallel.ForEach(requests, request => {
		request.AddCookie("ASP.NET_SessionId","cybtsmwddsxm4mmwludmcz5l");
		request.AddCookie("NSC_JOjwgzbmdd2su1se3dfnypb5ry4g1bQ","ffffffffaf091f2845525d5f4f58455e445a4a423660");
		request.AddCookie("SAW_SESSION_AUTHROLE","WR_WorkCenterMarketing");
		request.AddCookie("SAW_SESSION_AUTHURL","aHR0cHM6Ly93d3cuc3Nvc3RhZ2luZy5zZXJ2cHJvbmV0LmNvbTo0NDMvX0F1dGhBZ2VudHMvbW15SS1icGhUR1NJTFBEOGVQRl9CZy9BRF9MREFQ");
		request.AddCookie("SAW_SID", "JTxD8UG4qSF*w");
		request.AddCookie("franchise-roles","Users");
		request.AddCookie("role", "F_AllFranchiseOwners");
		
		client.Execute(request);
	
	});
}

// Define other methods and classes here

