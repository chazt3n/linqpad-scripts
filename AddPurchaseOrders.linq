<Query Kind="Statements">
  <Connection>
    <ID>a79d2235-06fb-49d3-a562-0955b48c46c2</ID>
    <Persist>true</Persist>
    <Server>cpsqlxiit01vn01\wcom</Server>
    <IsProduction>true</IsProduction>
    <Database>WorkCenters</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <NuGetReference>Autofac</NuGetReference>
  <NuGetReference>NBuilder</NuGetReference>
  <NuGetReference>Patterns.Autofac</NuGetReference>
  <NuGetReference>RestSharp</NuGetReference>
  <NuGetReference>System.Linq.Dynamic</NuGetReference>
  <Namespace>Autofac</Namespace>
</Query>

var purchaseOrder = PurchaseOrders.First();
purchaseOrder.Notes = "Here are some notes to print.";
var dt = DateTime.Now;
purchaseOrder.PurchaseOrderItems.Add(new PurchaseOrderItem(){Amount = 12, Quantity = 1, Description = "Item descrition describing the item..", PurchaseOrderId = purchaseOrder.Id, UpdatedDate = dt, CreatedDate = dt, UpdatedBy = "Chasten", CreatedBy="Chasten"});
purchaseOrder.PurchaseOrderItems.Add(new PurchaseOrderItem(){Amount = 24, Quantity = 1, Description = "Item descrition describing the item..", PurchaseOrderId = purchaseOrder.Id, UpdatedDate = dt, CreatedDate = dt, UpdatedBy = "Chasten", CreatedBy="Chasten"});

SubmitChanges();
purchaseOrder.Dump("Purchase Order");